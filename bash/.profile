PATH="$HOME/scripts:$PATH"
export XMODIFIERS=@im=ibus
export GTK_IM_MODULE=ibus
export QT_IM_MODULE=ibus
export CCACHE_DIR=/var/cache/ccache
export PATH
export VIRSH_DEFAULT_CONNECT_URI=qemu:///system
export LC_ALL=en_US.UTF-8

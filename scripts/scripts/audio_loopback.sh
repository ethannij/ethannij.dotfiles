#!/bin/bash

#create sink for mix
pactl load-module module-null-sink sink_name=mix-for-virtual-mic \
sink_properties=device.description=Mix-for-Virtual-Microphone

#create sink for application audio to speaker and mix
pactl load-module module-combine-sink sink_name=virtual-microphone-and-speakers \
slaves=mix-for-virtual-mic,alsa_output.usb-SteelSeries_SteelSeries_Arctis_7-00.analog-stereo

#loopback for mic
pactl load-module module-loopback source=alsa_input.usb-BLUE_MICROPHONE_Blue_Snowball_201306-00.multichannel-input \
sink=mix-for-virtual-mic latency_msec=20

#echo cancel fake
pactl load-module module-null-sink sink_name=silence \
sink_properties=device.description=silent-sink-for-echo-cancel

#real loopback
pactl load-module module-echo-cancel \
sink_name=virtual-microphone source_name=virtual-microphone \
source_master=mix-for-virtual-mic.monitor sink_master=silence aec_method=null \
source_properties=device.description=Virtual-Microphone \
sink_properties=device.description=Virtual-Microphone

#!/bin/bash
# first input is osu song dir
# second input is target dir
# inside osu song dir find all *.mp3
# copy all songs from osu dir to target dir

# Take Arguments
echo 'Input full path to "osu/Songs"'
read INPUT_DIR
echo 'Input full path to target folder'
read TARGET_DIR

# Move into Songs folder
cd $INPUT_DIR

mkdir -p $TARGET_DIR || exit 1
for pathname in "$INPUT_DIR"/*/*.mp3; do
    cp "$pathname" "$TARGET_DIR/$( basename "$( dirname "$pathname" )" ).mp3"
done
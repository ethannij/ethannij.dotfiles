### ethannij.dotfiles
# Instructions  
  
To get started, you need to install stow.  
Once you have stow, you can get started.  
- `git clone https://gitlab.com/ethannij/ethannij.dotfiles`
- Make sure you don't have an existing configuration file ex. `~/.zshrc`
- To use my dotfiles, enter `ethannij.dotfiles`
- `cd ethannij.dotfiles`
- Now you can stow my dotfiles
- `stow -t ~ zsh` for zsh

You should be all set to use my dotfiles!

The GTK and Firefox themes are found at https://draculatheme.com/
You must download the GTK dracula theme

The themes from this website match my theme for my dotfiles, so look through here and see what you like :)